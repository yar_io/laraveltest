<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\UserController;
use App\Models\Book;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookControllerTest extends TestCase
{

    protected $token;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createUserAndToken();
    }

    protected function createUserAndToken(): void
    {
        $user = User::factory()->create();
        $this->token = (new UserController())->getToken($user);
    }

    public function testIndexReturnsBookCollection()
    {
        $response = $this->get('/api/books/');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'author',
                    'genre',
                    'publication_year',
                    'price',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }


    public function testShowReturnsBookDetails()
    {
        $book = Book::factory()->create();

        $response = $this->get("/api/books/{$book->id}");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'author',
                'genre',
                'publication_year',
                'price',
                'created_at',
                'updated_at'
            ]
        ]);

        $response->assertJson([
            'data' => $book->toArray()
        ]);
    }


    public function testStoreAddsNewBook()
    {

        $data = [
            'title' => 'Test Book 234234243',
            'author' => 'Test Author',
            'genre' => 'Test Genre',
            'publication_year' => 2022,
            'price' => 19.99
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/books', $data);


        $response->assertStatus(201)
            ->assertJson([
                'data' => $data
            ]);

        $bookData = $response->json('data');

        $this->assertDatabaseHas('books', $bookData);
    }

    public function testUpdateUpdatesBookDetails()
    {
        $book = Book::factory()->create();

        $newData = [
            'title' => 'Updated Title',
            'author' => 'Updated Author',
            'genre' => 'Updated Genre',
            'publication_year' => 2023,
            'price' => 29.99,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put("/api/books/{$book->id}", $newData);

        $response->assertStatus(200)
            ->assertJson([
                'data' => $newData
            ]);

        $this->assertDatabaseHas('books', $newData);
    }

    public function testDestroyDeletesBook()
    {
        $book = Book::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete("/api/books/{$book->id}");

        $response->assertStatus(204);

        $this->assertDatabaseMissing('books', ['id' => $book->id]);
    }
}
