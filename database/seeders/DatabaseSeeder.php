<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\BookSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->createUser();

        $this->call([
            BookSeeder::class,
        ]);
    }


    /**
     * Create default user
     */
    private function createUser(): void
    {
        User::create([
            'name' => 'user',
            'email' => 'user@example.com',
            'password' => 'password',
        ]);
    }
}
