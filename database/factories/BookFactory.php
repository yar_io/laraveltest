<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $genres = ['Fiction', 'Non-fiction', 'Mystery', 'Romance', 'Science Fiction', 'Fantasy', 'Horror', 'Thriller', 'Biography', 'History', 'Self-help', 'Poetry', 'Cookbook', 'Travel', 'Children', 'Young Adult', 'Comics', 'Graphic Novel', 'Art', 'Science', 'Business', 'Philosophy', 'Religion', 'Sports'];

        return [
            'title' => $this->faker->sentence(3),
            'author' => $this->faker->name(),
            'genre' => $genres[array_rand($genres)],
            'publication_year' => $this->faker->numberBetween(1900, date('Y')),
            'price' => $this->faker->randomFloat(2, 10, 100),
        ];
    }
}
