<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Generate Bearer Token
     */
    function getToken(User $user): string
    {
        return $user->createToken('token')->plainTextToken;
    }
}
