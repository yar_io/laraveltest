<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\JsonResponse;

/**
 * @group Books
 *
 * APIs for managing books
 */
class BookController extends Controller
{
    /**
     * Display a listing of the books.
     *
     * @response \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $books = Book::all();

        return BookResource::collection($books);
    }

    /**
     * Store a newly created book in storage.
     *
     * @param  \App\Http\Requests\StoreBookRequest  $request
     * @return \App\Http\Resources\BookResource
     *
     * @response \App\Http\Resources\BookResource
     */
    public function store(StoreBookRequest $request): BookResource
    {
        $book = Book::create($request->validated());

        return new BookResource($book);
    }

    /**
     * Display the book by its ID.
     *
     * @param  \App\Models\Book  $book
     * @return \App\Http\Resources\BookResource
     *
     * @response \App\Http\Resources\BookResource
     */
    public function show(Book $book): BookResource
    {
        return new BookResource($book);
    }

    /**
     * Update the specified book by ID.
     *
     * @param  \App\Http\Requests\UpdateBookRequest  $request
     * @param  \App\Models\Book  $book
     * @return \App\Http\Resources\BookResource
     *
     * @response \App\Http\Resources\BookResource
     */
    public function update(UpdateBookRequest $request, Book $book): BookResource
    {
        $book->update($request->validated());

        return new BookResource($book);
    }

    /**
     * Remove the specified book from database.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\JsonResponse
     *
     * @response 204 {}
     */
    public function destroy(Book $book): JsonResponse
    {
        $book->delete();

        return response()->json([], 204);
    }
}
