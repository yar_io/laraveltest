<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Junges\Kafka\Contracts\ConsumerMessage;
use Junges\Kafka\Contracts\MessageConsumer;
use Junges\Kafka\Facades\Kafka;

class KafkaConsumer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kafka:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume messages from Kafka topics';

    /**
     * Execute the console command.
     * @throws Exception
     * @throws \Carbon\Exceptions\Exception
     */
    public function handle(): void
    {
        $consumer = Kafka::consumer(['my-topic'])
            ->withBrokers('kafka:9092')
            ->withAutoCommit()
            ->withHandler(function(ConsumerMessage $message, MessageConsumer $consumer) {
                dump($message->getBody());
            })
            ->build();

        $consumer->consume();
    }
}
