<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;

//use RdKafka;

class KafkaProducer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kafka:produce {message*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Produces messages to Kafka topics';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle(): void
    {
        try {
            $producer = Kafka::publish('kafka:9092')
                ->onTopic('my-topic');

            $message = new Message(
                body: $this->argument('message'),
            );
            $producer->withMessage($message);
            $producer->send(true);

//            for ($i = 0; $i < 100; $i++) {
//                $message = new Message(
//                    body: rand(1, 1000),
//                );
//
//                $producer->withMessage($message);
//                $producer->send(true);
//            }


        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
