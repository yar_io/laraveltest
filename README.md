````php artisan migrate````

````php artisan db:seed````

Generate example user token route - /api/get-token/1

Generate API docs - 

```php artisan idoc:generate```

API Docs route - /api/docs
